import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {
  ButtonModule,
  CheckboxModule,
  ConfirmationService,
  ConfirmDialogModule,
  ContextMenuModule,
  DataTableModule,
  DialogModule,
  DropdownModule,
  GMapModule,
  GrowlModule,
  InputMaskModule,
  MessagesModule,
  OrderListModule,
  OverlayPanelModule,
  PanelMenuModule,
  PanelModule,
  SharedModule
} from 'primeng/primeng';
import {AccueilComponent} from './shared/cadre/accueil/accueil.component';
import {MenugaucheComponent} from './shared/cadre/menugauche/menugauche.component';
import {NavbarComponent} from './shared/cadre/navbar/navbar.component';
import {FootbarComponent} from './shared/cadre/footbar/footbar.component';
import {InviteComponent} from './personne/invite/invite.component';
import {EtudiantComponent} from './personne/etudiant/etudiant.component';
import {AdminComponent} from './personne/admin/admin.component';
import {EmployeComponent} from './personne/employe/employe.component';
import {AlertModule} from 'ngx-bootstrap';
import {InviteService} from './shared/service/personne/invites/invite.service';
import {EtudiantDetailComponent} from './personne/etudiant/etudiant-detail/etudiant-detail.component';
import {EtudiantEditComponent} from './personne/etudiant/etudiant-edit/etudiant-edit.component';
import {EtudiantCreateComponent} from './personne/etudiant/etudiant-create/etudiant-create.component';
import {EtudiantListComponent} from './personne/etudiant/etudiant-list/etudiant-list.component';
import 'hammerjs';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {MatiereComponent} from './matiere/matiere.component';
import {MatiereEditComponent} from './matiere/matiere-edit/matiere-edit.component';
import {MatiereCreateComponent} from './matiere/matiere-create/matiere-create.component';
import {MatieresListComponent} from './matiere/matieres-list/matieres-list.component';
import {MatiereService} from './shared/service/matiere/matiere.service';
import {MatiereManageComponent} from './matiere/matiere-manage/matiere-manage.component';
import {EnseignantModule} from './personne/enseignant/enseignant.module';
import { MatiereDetailComponent } from './matiere/matiere-detail/matiere-detail.component';
import {AuthMatiereGuard} from './shared/guard/matiere/auth-matiere.guard';
import {CanDesactiveMatiereGuard} from './shared/guard/matiere/can-desactive-matiere.guard';
import {MatiereDetailResolveService} from './shared/service/matiere/resolve/matiere-detail-resolve.service';
import { MatiereNotFindComponent } from './matiere/matiere-not-find/matiere-not-find.component';
import {MatCardModule} from '@angular/material';
import {MatiereInviteComponent} from './matiere/matiere-invite/matiere-invite.component';


@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    MenugaucheComponent,
    NavbarComponent,
    FootbarComponent,
    InviteComponent,
    EtudiantComponent,
    AdminComponent,
    EmployeComponent,
    EtudiantDetailComponent,
    EtudiantEditComponent,
    EtudiantCreateComponent,
    EtudiantListComponent,
    MatiereComponent,
    MatiereEditComponent,
    MatiereCreateComponent,
    MatieresListComponent,
    MatiereManageComponent,
    MatiereDetailComponent,
    MatiereNotFindComponent,
    MatiereInviteComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ButtonModule, PanelModule, PanelMenuModule,
    DataTableModule, DropdownModule, InputMaskModule, GMapModule, MessagesModule, OrderListModule,
    SharedModule, DialogModule, MessagesModule, GrowlModule, CheckboxModule, OverlayPanelModule,
    ConfirmDialogModule, ContextMenuModule,
    AlertModule.forRoot(),
    EnseignantModule,
    MatCardModule
  ],
  providers: [InviteService,
    ConfirmationService,
    MatiereService, AuthMatiereGuard, CanDesactiveMatiereGuard, MatiereDetailResolveService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
