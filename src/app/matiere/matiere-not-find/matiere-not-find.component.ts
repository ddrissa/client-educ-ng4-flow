import {Component, OnInit} from '@angular/core';
import {MatiereService} from '../../shared/service/matiere/matiere.service';

@Component({
  selector: 'app-matiere-not-find',
  templateUrl: './matiere-not-find.component.html',
  styleUrls: ['./matiere-not-find.component.scss']
})
export class MatiereNotFindComponent implements OnInit {
  mmessage: String[];

  constructor(private matiereService: MatiereService) {
  }

  ngOnInit() {
    this.matiereService.matiereNotFind$.subscribe(msg => this.mmessage = msg);
  }

}
