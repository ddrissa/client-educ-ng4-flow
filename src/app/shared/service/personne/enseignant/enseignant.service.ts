import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Resultat} from '../../../model/resultat';
import {Enseignant} from '../../../model/personne/enseignant';
import {Adresse} from '../../../model/personne/adresse';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class EnseignantService {
  private urlEnseigns = 'http://localhost:8080/typepersonnes/EN';
  private _url = 'http://localhost:8080/personnes';
  // photos url pathphoto 1
  private _url_photos = 'http://localhost:8080/photos/';
  // photos url photo 2
  private _url_photos2 = 'http://localhost:8080/photo/';
  // photos url photo recuperer maniere 2
  private _url_photo = 'http://localhost:8080/photo2/';
  private _url_rech = 'http://localhost:8080/persrech/EN?mc=';

  // Enseignant source
  private enseignantCreerSource = new Subject<Resultat<Enseignant>>();
  private enseignantModifSource = new Subject<Resultat<Enseignant>>();
  private enseignantSupprimeSource = new Subject<Resultat<boolean>>();
  private enseignantFilterSource = new Subject<string>();
  private reponseStatutSource = new Subject<number>();

  // Stream
  enseignantCreer$ = this.enseignantCreerSource.asObservable();
  enseignantModifier$ = this.enseignantModifSource.asObservable();
  enseignantSupprime$ = this.enseignantSupprimeSource.asObservable();
  enseignantFilter$ = this.enseignantFilterSource.asObservable();
  reponseStatut$ = this.reponseStatutSource.asObservable();

  constructor(private _http: Http) {
  }

// La liste de tous les enseignant
  getAllEnseignant(): Observable<Resultat<Array<Enseignant>>> {
    return this._http.get(this.urlEnseigns)
      .map(res => res.json())
      .do(data => this.filtrerText(' '))
      .catch(this._errorHandler);
  }


  // Ajouter un enseignant
  ajoutEnseignant(ens: Enseignant): Observable<Resultat<Enseignant>> {
    // chemin photo
    // const pathph = `${this._url_photos}${ens.numCni}`;
    // L'adresse de l'enseignant
    const adr = new Adresse(ens.adresse.quartier, ens.adresse.codePostal,
      ens.adresse.email, ens.adresse.mobile, ens.adresse.bureau, ens.adresse.tel);
    // L'enseignant cree avec l'adresse;
    const newEnseig = new Enseignant(null, 0, ens.titre, adr, ens.nom, ens.prenom,
      ens.numCni, ens.login, ens.password, ens.actived, 'EN', ens.pathphoto, ens.telephones, ens.nomComplet, ens.status,
      ens.specialite);


    return this._http.post(this._url, newEnseig)
      .map(res => res.json())
      .do(data => {
        this.filtrerText(data.body.nomComplet);
        this.statutReponse(data.status);
        this.enseignantCreer(data);
        console.log('Enseignant creé', data);
      })
      .catch(this._errorHandler);
  }


  // Modifier un enseignant
  modifierEnseignant(ens: Enseignant): Observable<Resultat<Enseignant>> {
    // chemin photo
    //  const pathph = `${this._url_photos}${ens.numCni}`;
    // L'adresse de l'enseignant
    const adr = new Adresse(ens.adresse.quartier, ens.adresse.codePostal,
      ens.adresse.email, ens.adresse.mobile, ens.adresse.bureau, ens.adresse.tel);
    // L'enseignant cree avec l'adresse;
    const modEns = new Enseignant(ens.id, ens.version, ens.titre, adr, ens.nom, ens.prenom,
      ens.numCni, ens.login, ens.password, ens.actived, 'EN', ens.pathphoto, ens.telephones, ens.nomComplet, ens.status,
      ens.specialite);
    return this._http.put(this._url, modEns)
      .map(res => res.json())
      .do(data => {
        this.filtrerText(data.body.nomComplet);
        this.enseignantModif(data);
        console.log('Enseignant modifier ', data);
      })
      .catch(this._errorHandler);

  }

  // Trouver un enseignant par id
  getEnseignatById(id: number): Observable<Resultat<Enseignant>> {
    return this._http.get(`${this._url}/${id}`)
      .map(res => res.json())
      .do(res => console.log('Enseignant retrouvé par id: ', res))
      .catch(this._errorHandler);
  }

  // Trouver un ensegnant par motcle
  searchEnsMotcle(mc: string): Observable<Enseignant[]> {
    return this._http.get(`${this._url_rech}${mc}`)
      .map(res => res.json().body)
      .do(data => console.log('Enseignant retrouvé par id: ', data))
      .catch(this._errorHandler);
  }

  // Supprimer un enseignant
  supprimerEnseignat(id: number): Observable<Resultat<boolean>> {
    return this._http.delete(`${this._url}/${id}`)
      .map(res => res.json())
      .do(data => {
        this.filtrerText(' ');
        this.enseignantSuprime(data);
        console.log('Enseignant spprimer : ', data);
      })
      .catch(this._errorHandler);
  }

// Recuperer le lien d'image pour <img src=getPhoto(id)
  getPhotos(cni: string) {
    return `${this._url_photos}${cni}`;

  }

  // recuperer une pathphoto par reponse
  getPhoto(cni: string): Observable<Resultat<File>> {
    return this._http.get(`${this._url_photo}${cni}`)
      .map(res => res.json())
      .do(res => console.log('Photo recuperer: ', res))
      .catch(this._errorHandler);

  }

  // Ajouter une pathphoto a l'enseignnt
  ajoutPhoto(file: File, cni: string): Observable<Resultat<Enseignant>> {
    // Forme de donnee du formulaire
    const data = new FormData();

    data.append('imageFile', file, cni);

    return this._http.post(this._url_photos, data)
      .map(res => res.json())
      .do(d => {
        this.filtrerText(d.body.nomComplet);
        this.enseignantModif(d);
        console.log('Photo ajouter à: ', d);
      })
      .catch(this._errorHandler);
  }

  // Creer un enseignant avec la pathphoto
  creerEnseignPhoto(file: File, ens: Enseignant): Observable<Resultat<Enseignant>> {
    const fdata = new FormData();
    fdata.append('enseignant', ens);
    fdata.append('image', file, ens.numCni);
    return this._http.post(this._url_photos2, fdata)
      .map(res => res.json())
      .do(data => {
        this.enseignantModif(data);
        console.log('Service enregistrer avec sa pathphoto: ', data);
      })
      .catch(this._errorHandler);

  }

/////////////////////////Sources///////////////////////////
  private enseignantCreer(r: Resultat<Enseignant>) {
    this.enseignantCreerSource.next(r);
  }

  private enseignantModif(r: Resultat<Enseignant>) {
    this.enseignantModifSource.next(r);
  }

  private enseignantSuprime(r: Resultat<boolean>) {
    this.enseignantSupprimeSource.next(r);
  }

  private filtrerText(t: string) {
    this.enseignantFilterSource.next(t);
  }

  private statutReponse(num: number) {
    this.reponseStatutSource.next(num);
  }

  /////////////////////////////////////////////////////////////
  // manipuler les erreurs

  _errorHandler(err) {
    let errMessage: string;
    if (err instanceof Response) {
      const body = err.json() || '';
      const erreur = body.error || JSON.stringify(body);
      errMessage = `${err.status} - ${err.statusText} || ''] ${erreur}`
      ;
    } else {
      errMessage = err.message ? err.message : err.toString();

    }

    return Observable.throw(errMessage);
  }
}
