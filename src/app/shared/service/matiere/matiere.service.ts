import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Resultat} from '../../model/resultat';
import {Matiere} from '../../model/matiere/matiere';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class MatiereService {

  private _url = 'http://localhost:8080/matieres';
  private _url_search = 'http://localhost:8080/matrech?mc=';
  // observable source.
  private matiereCreerSource = new Subject<Resultat<Matiere>>();
  private matiereModifierSource = new Subject<Resultat<Matiere>>();
  private matiereSupprimerSource = new Subject<Matiere>();
  private matiereSelectedSource = new Subject<Matiere>();
  private matiereFiltreSource = new Subject<string>();
  private matiereNotFindSource = new Subject<string[]>();

  // observable stream.
  matiereCreer$ = this.matiereCreerSource.asObservable();
  matiereModifier$ = this.matiereModifierSource.asObservable();
  matiereSupprimer$ = this.matiereSupprimerSource.asObservable();
  matiereSelected$ = this.matiereSelectedSource.asObservable();
  matiereFiltre$ = this.matiereFiltreSource.asObservable();
  matiereNotFind$ = this.matiereNotFindSource.asObservable();

  constructor(private _http: Http) {
  }

///////////////////////////////////////
  /*
   getAlllMatiere
   * */
  getAllMatieres(): Observable<Resultat<Array<Matiere>>> {
    return this._http.get(this._url)
      .map(res => res.json())
      .do(data => console.log('Charger'))
      .catch(this._errorHandler);


  }

  getMatiere(id: number): Observable<Resultat<Matiere>> {
    return this._http.get(`${this._url}/${id}`)
      .map(res => res.json())
      .do(data => console.log(data))
      .catch(this._errorHandler);
  }

// Rechercher par mot clé
  seachMatiere(mc: string): Observable<Resultat<Array<Matiere>>> {
    return this._http.get(`${this._url_search}${mc}`)
      .map(res => res.json())
      .do(data => console.log(data))
      .catch(this._errorHandler);
  }


  sauver(mat: Matiere): Observable<Resultat<Matiere>> {
    /* const mati: Imatiere = {
     libelle: mat.libelle,
     description: mat.description
     };*/
    return this._http.post(this._url, mat)
      .map(res => res.json())
      .do(data => {
        this.matiereCreer(data);
        this.filtreText(data.body.libelle);
      })
      .catch(this._errorHandler);
  }

  modifierMatiere(mat: Matiere): Observable<Resultat<Matiere>> {
    return this._http.put(this._url, mat)
      .map(res => res.json())
      .do(data => {
        this.matiereModifier(data);
        this.filtreText(mat.libelle);
      })
      .catch(this._errorHandler);
  }

  supprimeerMatiere(id: number): Observable<Resultat<boolean>> {
    return this._http.delete(`${this._url}/${id}`)
      .map(res => res.json())
      .do(data => {
        this.filtreText(data.body.libelle);
        console.log('Service supprime', data);
      })
      .catch(this._errorHandler);
  }

// Une matiere a été creé on ajoute l'indfo à notre stream
  matiereCreer(res: Resultat<Matiere>) {
    console.log('matiere a été creé');
    this.matiereCreerSource.next(res);
  }

// Une matiere a été modifier, on ajoute l'indfo notre stream
  matiereModifier(res: Resultat<Matiere>) {
    console.log('matiere a été modifier');
    this.matiereModifierSource.next(res);
  }

// Une matiere a été supprimer, on ajoute l'indfo notre stream
  matiereSupprimer(mat: Matiere) {
    this.matiereSupprimerSource.next(mat);
  }

  matiereSelectStream(m: Matiere) {
    this.matiereSelectedSource.next(m);
  }

  filtreText(t: string) {
    this.matiereFiltreSource.next(t);
  }
  notfind(msg: string[]) {
    this.matiereNotFindSource.next(msg);
  }

/////////////////////////////////////////////////////////////
// manipuler les erreurs

  _errorHandler(err) {
    let errMessage: string;
    if (err instanceof Response) {
      const body = err.json() || '';
      const erreur = body.error || JSON.stringify(body);
      errMessage = `${err.status} - ${err.statusText} || ''] ${erreur}`;
    } else {
      errMessage = err.message ? err.message : err.toString();

    }

    return Observable.throw(errMessage);
  }
}
