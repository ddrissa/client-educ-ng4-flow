import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {InviteComponent} from 'app/personne/invite/invite.component';
import {EtudiantComponent} from './personne/etudiant/etudiant.component';
import {EmployeComponent} from 'app/personne/employe/employe.component';
import {AdminComponent} from './personne/admin/admin.component';
import {AccueilComponent} from './shared/cadre/accueil/accueil.component';
import {EtudiantListComponent} from './personne/etudiant/etudiant-list/etudiant-list.component';
import {MatieresListComponent} from './matiere/matieres-list/matieres-list.component';
import {MatiereEditComponent} from './matiere/matiere-edit/matiere-edit.component';
import {MatiereCreateComponent} from './matiere/matiere-create/matiere-create.component';
import {MatiereManageComponent} from './matiere/matiere-manage/matiere-manage.component';
import {MatiereDetailComponent} from './matiere/matiere-detail/matiere-detail.component';
import {AuthMatiereGuard} from './shared/guard/matiere/auth-matiere.guard';
import {CanDesactiveMatiereGuard} from './shared/guard/matiere/can-desactive-matiere.guard';
import {MatiereDetailResolveService} from './shared/service/matiere/resolve/matiere-detail-resolve.service';
import {MatiereInviteComponent} from './matiere/matiere-invite/matiere-invite.component';

const routes: Routes = [
    {
      path: '', redirectTo: '/accueil', pathMatch: 'full'

    },
    {
      path: 'accueil', component: AccueilComponent
    },
    {
      path: 'invite', component: InviteComponent
    },
    {
      path: 'etudiant',
      component: EtudiantComponent,
      children: [
        {
          path: ' ',
          component: EtudiantListComponent
        }
      ]
    },
    {
      path: 'employe', component: EmployeComponent
    },
    {
      path: 'admin', component: AdminComponent
    },
    {
      path: 'matiere',
      children: [
        {
          path: '', component: MatiereManageComponent
        },

        {
          path: 'list',
          canActivate: [AuthMatiereGuard],
          component: MatieresListComponent,
          canActivateChild: [AuthMatiereGuard],
          children: [
            {path: '', component: MatiereInviteComponent},
            {
              path: 'detail/:id', component: MatiereDetailComponent,
              resolve: {resultat: MatiereDetailResolveService}
            },
            {
              path: 'edit/:id', component: MatiereEditComponent,
              canDeactivate: [CanDesactiveMatiereGuard]
            },
            {
              path: 'create', component: MatiereCreateComponent,
              canDeactivate: [CanDesactiveMatiereGuard]
            }
          ]
        },

      ]
    }
  ]
;

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
