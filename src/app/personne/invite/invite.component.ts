import {Component, OnInit} from '@angular/core';
import {InviteService} from '../../shared/service/personne/invites/invite.service';
import {Invite} from '../../shared/model/personne/invite';
import {Iinvite} from '../../shared/model/personne/interface/personne/iinvite';
import {Adresse} from '../../shared/model/personne/adresse';

import {ConfirmationService, MenuItem, Message, SelectItem} from 'primeng/primeng';

@Component({
  selector: 'app-invite',
  templateUrl: './invite.component.html',
  styleUrls: ['./invite.component.scss']
})
export class InviteComponent implements OnInit {
  invites: Invite[] = [];
  statusSucces: number;
  invite: Invite;
  selectedInvite: Invite;
  displayDialog: boolean;
  dialogVisible: boolean;
  newInvite: boolean;
  succesMessage: string;
  succesErreurMessage: string;
  errorMessage: string;
  errorMessageStatus: string;
  // selectTtems
  titres: SelectItem[];
  statuts: SelectItem[];
  profils: SelectItem[];
  // menuItem
  items: MenuItem[];
  msgs: Message[];

  loading: boolean;

  constructor(private  inviteService: InviteService, private confirmationService: ConfirmationService) {
  }

  ngOnInit() {
    this.loading = true;
    setTimeout(() => {
      this.tousInvites();
      this.loading = false;
    }, 1000);

    this.selectTitres();
    this.selectStatuts();
    this.selectProfils();

    this.items = [
      {label: 'Supprimer', icon: 'fa-close', command: (event) => this.deleteInvite(this.selectedInvite)}
    ];
  }

  tousInvites() {
    this.inviteService.getAllsInvites()
      .subscribe(data => {
          this.invites = data.body;
          /*this.statusSucces = data.status;
           this.succesMessage = data.messages.toString();*/
        },
        error => {
          if (error.status = 0) {
            this.errorMessageStatus = 'Problème de conxion!';
          } else {
            this.errorMessage = error;
          }
        });
  }


  showDialogToAdd() {
    this.newInvite = true;

    const ad = new Adresse('', '', '', '', '', '');
    this.invite = new Invite(null, null, '', ad, null,
      null, null, null, null, false, null,
      null, null, null, null, null);
    this.displayDialog = true;
  }

  cancel() {
    this.displayDialog = false;
    this.dialogVisible = false;
  }

  showInvite(inv: Invite) {
    this.newInvite = false;
    this.selectedInvite = inv;
    this.invite = this.cloneInvite(inv);
    this.dialogVisible = true;
    // this.invite = this.selectedInvite;
  }

  save() {
    const invites = [...this.invites];
    if (this.newInvite) {
      this.inviteService.ajoutInvite(this.invite)
        .subscribe(res => {
            if (res.status === 0) {
              invites.push(res.body);
              this.invites = invites;
              this.statusSucces = res.status;
              this.succesMessage = res.messages.toString();
              // this.invites = invites;
            } else {
              this.statusSucces = res.status;
              this.succesErreurMessage = res.messages.toString();
              console.log(res);
            }
          },
          error => this.errorMessage = error);

    } else {
      console.log('modif debut');
      console.log(this.invite);
      this.inviteService.editerInvitet(this.invite)
        .subscribe(res => {
            if (res.status === 0) {
              invites[this.findSelectedInviteIndex()] = res.body;
              this.invites = invites;
              this.statusSucces = res.status;
              this.succesMessage = res.messages.toString();
            } else {
              this.statusSucces = res.status;
              this.succesMessage = res.messages.toString();
            }
          },
          resError => {
            this.errorMessage = resError;
            console.error(resError);
          }
        );
    }

    this.invite = null;
    this.displayDialog = false;
    this.dialogVisible = false;
    this.closeMessage();
    console.log(this.statusSucces);
    console.log(this.succesMessage);

  }

  delete() {
    const index = this.findSelectedInviteIndex();
    this.inviteService.suppimeInvite(this.invite.id)
      .subscribe(res => {
        if (res.status === 0) {
          this.succesMessage = res.messages.toString();
          this.statusSucces = res.status;
          this.invites = this.invites.filter((val, i) => i !== index);
          console.log(res);
        } else {
          this.succesMessage = res.messages.toString();
          this.statusSucces = res.status;
          console.log(res);
        }
      }, erreur => this.errorMessage = erreur);
    this.invite = null;
    this.displayDialog = false;
    this.closeMessage();
  }

  onRowSelect(event) {
    this.newInvite = false;
    this.invite = this.cloneInvite(event.data);
    this.displayDialog = true;
    console.log(event.data);
    console.log(this.invite);
  }

  findSelectedInviteIndex(): number {
    return this.invites.indexOf(this.selectedInvite);
  }

  cloneInvite(i: Iinvite): Invite {
    // cloner adresse
    let add: Adresse;
    add = new Adresse(i.adresse.quartier,
      i.adresse.codePostal,
      i.adresse.email,
      i.adresse.mobile,
      i.adresse.bureau,
      i.adresse.tel);
    // cloner invite
    let invite: Invite;
    invite = new Invite();
    for (const prop in i) {
      invite[prop] = i[prop];
    }
    invite.adresse = add;
    return invite;
  }

  private closeMessage() {
    setTimeout(() => {
      this.succesMessage = '';
      this.succesErreurMessage = '';
      this.errorMessage = '';
      this.errorMessageStatus = '';
      this.statusSucces = null;
    }, 5000);
  }

  // pour DropDowns
  selectTitres() {
    this.titres = [];
    this.titres.push({label: 'Mme', value: 'Mme'});
    this.titres.push({label: 'Mlle', value: 'Mlle'});
    this.titres.push({label: 'Mr', value: 'Mr'});
    this.titres.push({label: 'Dr', value: 'Dr'});
    this.titres.push({label: 'Me', value: 'Me'});
  }

  selectStatuts() {
    this.statuts = [];
    this.statuts.push({label: 'Temporaire', value: 'Tempo'});
    this.statuts.push({label: 'Permanant', value: 'Perm'});
  }

  selectProfils() {
    this.profils = [];
    this.profils.push({label: 'Client', value: 'Client'});
    this.profils.push({label: 'Partenaire', value: 'Partenaire'});
    this.profils.push({label: 'Visiteur', value: 'Visiteur'});
  }

  deleteInvite(inv: Invite) {
    this.confirmDelete(inv);
  }

  confirmDelete(inv: Invite) {
    this.confirmationService.confirm({
      message: `Voulez-vous suppimer l'invité de ID:${inv.id} ${inv.numCni} ${inv.nom} ${inv.prenom}`,
      header: 'Confirmation de suppression',
      icon: 'fa-fa-trash',
      accept: () => {
        this.msgs = [];
        this.selectedInvite = inv;
        this.invite = this.selectedInvite;
        console.log('Confirme');
        console.log(this.invite)
        this.delete();
        this.msgs.push({severity: 'info', summary: 'Comfirmation', detail: 'Visiteur supprimé.'});
      }
    });
  }

}
