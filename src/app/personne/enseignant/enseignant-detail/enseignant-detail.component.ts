import {Component, OnInit} from '@angular/core';
import {EnseignantService} from '../../../shared/service/personne/enseignant/enseignant.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Enseignant} from '../../../shared/model/personne/enseignant';

@Component({
  selector: 'app-enseignant-detail',
  templateUrl: './enseignant-detail.component.html',
  styleUrls: ['./enseignant-detail.component.scss']
})
export class EnseignantDetailComponent implements OnInit {
  enseignant: Enseignant;
  pathNullImage = './assets/image/ensNull.png';

  constructor(private enseignantService: EnseignantService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.route.params.switchMap((params: Params) => this.enseignantService.getEnseignatById(+params['id']))
      .subscribe(res => {
        this.enseignant = res.body;
        console.log('Enseignant trouer ', res.body);
      });
  }

  editer() {
    this.router.navigate(['edit'], {relativeTo: this.route});
  }

  supprimer(e: Enseignant) {
    this.enseignantService.supprimerEnseignat(e.id)
      .subscribe(res => {
        console.log('Suppression :', res);
      });
    this.router.navigate(['enseignant/list']);
  }

  photo() {
    this.router.navigate(['photo'], {relativeTo: this.route});
  }
}
