import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {EnseignantService} from '../../../shared/service/personne/enseignant/enseignant.service';
import {Enseignant} from '../../../shared/model/personne/enseignant';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-enseignant-photo',
  templateUrl: './enseignant-photo.component.html',
  styleUrls: ['./enseignant-photo.component.scss']
})
export class EnseignantPhotoComponent implements OnInit {
  enseignant: Enseignant;
  succesErreur: string;
  photogroup: FormGroup;
  imageEnseignantFile: File;
  @ViewChild('imageEns') ens_image;

  constructor(private  enseignantService: EnseignantService,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.route.paramMap
      .switchMap((params: ParamMap) => this.enseignantService.getEnseignatById(+params.get('id')))
      .subscribe(res => {
        if (res.status === 0) {
          this.enseignant = res.body;
          this.initForm();
        } else {
          this.succesErreur = res.messages.toString();
        }
      });
  }


  onSubmit() {
    const image = this.ens_image.nativeElement;
    if (image.files && image.files[0]) {
      this.imageEnseignantFile = image.files[0];
    }
    const imageFile = this.imageEnseignantFile;
    // console.log(imageFile);
    this.enseignantService.ajoutPhoto(imageFile, this.enseignant.numCni)
      .subscribe(res => {
        console.log('Apres ajout de photo', res);
      });

    this.annuler();
  }

  annuler() {
    this.router.navigate(['enseignant/list']);
  }


  private initForm() {
    const cniEns: Enseignant = this.enseignant.numCni;
    this.photogroup = this.fb.group({
      cni: [cniEns],
      imageEns: ['', Validators.required]
    });
  }


}
