import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnseignantManageComponent } from './enseignant-manage.component';

describe('EnseignantManageComponent', () => {
  let component: EnseignantManageComponent;
  let fixture: ComponentFixture<EnseignantManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnseignantManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnseignantManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
