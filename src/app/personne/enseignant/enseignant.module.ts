import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {EnseignantRoutingModule} from './enseignant-routing.module';
import {EnseignantListComponent} from './enseignant-list/enseignant-list.component';
import {EnseignantEditComponent} from './enseignant-edit/enseignant-edit.component';
import {EnseignantDetailComponent} from './enseignant-detail/enseignant-detail.component';
import {EnseignantComponent} from './enseignant.component';
import {EnseignantDebutComponent} from './enseignant-debut/enseignant-debut.component';
import {EnseignantManageComponent} from './enseignant-manage/enseignant-manage.component';
import {EnseignantService} from '../../shared/service/personne/enseignant/enseignant.service';
import {EnseignantPhotoComponent} from './enseignant-photo/enseignant-photo.component';
import {
  MatButtonModule, MatButtonToggleModule,
  MatCardModule,
  MatChipsModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatProgressBarModule,
  MatSelectModule,
  MatSidenavModule, MatSnackBar, MatSnackBarModule,
  MatToolbarModule,
  MatTooltipModule
} from '@angular/material';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    EnseignantRoutingModule,
    // BrowserAnimationsModule,
    MatButtonModule, MatIconModule,
    MatSidenavModule, MatToolbarModule,
    MatTooltipModule, MatGridListModule,
    MatProgressBarModule, MatSelectModule,
    MatChipsModule, MatCardModule, MatInputModule,
    MatExpansionModule, MatSnackBarModule, MatButtonToggleModule
  ],
  declarations: [
    EnseignantListComponent,
    EnseignantEditComponent,
    EnseignantDetailComponent,
    EnseignantComponent,
    EnseignantDebutComponent,
    EnseignantManageComponent,
    EnseignantPhotoComponent],
  providers: [
    EnseignantService
  ]
})
export class EnseignantModule {
}
